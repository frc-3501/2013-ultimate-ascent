/*--------------------------------------------------------------------*/
/* Copyright (c) FIRST 2008. All Rights Reserved.                     */
/* Open Source Software - may be modified and shared by FRC teams.    */
/* The code must be accompanied by the FIRST BSD license file in the  */
/* root directory of the project.                                     */
/*--------------------------------------------------------------------*/
//#currylover101
//dannyisabeast

package edu.wpi.first.wpilibj.templates;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.DriverStationLCD;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.Jaguar;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class RobotTemplate extends IterativeRobot {
    /****     GENERAL     ****/
    Timer timer;
    
        // port variables
        int PORT_1 = 1, PORT_2 = 2, PORT_3 = 3, PORT_4 = 4, PORT_5 = 5,
            PORT_6 = 6, PORT_7 = 7, PORT_8 = 8, PORT_9 = 9, PORT_10 = 10;

    /****     SHOOTER     ****/
        // operation
        double lastIncHit;
        double increment;
        double shooterInc;
        double numShooterSpeedBumps;
        final double BUMPVALUE = 0.05;

        // piston
        DoubleSolenoid shooterSolenoid;
        Timer pistonTimer;
        
            // note: these ports are shooterSolenoid ports on
            // the breakout board, module 9472, CRIO slot 3
        final int dsPistonForward = 7, dsPistonReverse = 6;
        boolean pistonOut = false;

        // wheels
        Jaguar shooter;

    /****     COMPRESSOR     ****/
    Compressor compressor;
    
    final int compressorRelayChannel = 1, pressureSwitchChannel = 1;

    /****     DRIVER STATION     ****/
    Joystick rightStick, leftStick;
    DriverStationLCD lcd = DriverStationLCD.getInstance();
    DriverStation ds = DriverStation.getInstance();

        // joystick buttons
        final int   JBUTTON_1 = 1,
                    JBUTTON_2 = 2,
                    JBUTTON_3 = 3,
                    JBUTTON_4 = 4,
                    JBUTTON_5 = 5,
                    JBUTTON_6 = 6,
                    JBUTTON_7 = 7,
                    JBUTTON_8 = 8,
                    JBUTTON_9 = 9,
                    JBUTTON_10 = 10,
                    JBUTTON_11 = 11,
                    JBUTTON_12 = 12;

    final int JOYSTICK_RIGHT = 1, JOYSTICK_LEFT = 2;

    /****     DRIVE BASE     ****/
    RobotDrive robotDrive;
    
    double previousLeftDriveSpeed, previousRightDriveSpeed, currSpeed;
    final double backOfPyramidSpeed = 0.645;

    /****     CLIMBER     ****/
    Jaguar leftClimber, rightClimber;
    DoubleSolenoid climberSolenoid;
    Timer climberTimer;
    
    boolean climbing = false;
    double climberCoefficient;
    final int climberForward = 4, climberReverse = 5;
    boolean climberPistonOut = false;

    public void robotInit() {
        // various initial variable settings
        numShooterSpeedBumps = 0;
        lastIncHit = 0;
        currSpeed = 0;
        climberCoefficient = 1;
        previousLeftDriveSpeed = 0;
        previousRightDriveSpeed = 0;
        
        // new mechanisms
        shooterSolenoid = new DoubleSolenoid(1, 2);
        climberSolenoid = new DoubleSolenoid(4, 5);
        shooter = new Jaguar(PORT_3);
        rightStick = new Joystick(JOYSTICK_RIGHT);
        leftStick = new Joystick(JOYSTICK_LEFT);
        robotDrive = new RobotDrive(2, 1);
        compressor = new Compressor(pressureSwitchChannel,
                compressorRelayChannel);
        
        // this has run correctly
        print("robotInit completed", DriverStationLCD.Line.kUser4, true);
    }
    
    public void autonomousInit() {
        // new mechanisms
        timer = new Timer();
        
        // begin mechanism operation
        timer.start();
        compressor.start();
    }

    public void autonomousPeriodic() {
        setBothShooterSpeeds(backOfPyramidSpeed);
        
        // gets the elapsed time (in ms) every time autonomousPeriodic()
        // is called by the cRIO
        double time = timer.get(); // seconds, not ms
        if (((time > 5 && time < 5.10) || (time > 8 && time < 8.10) ||
                (time > 11 && time < 11.10))) {
            operateSolenoid(true); // this fires the solenoid for 0.1 secs every
                                   // three seconds after the shooter warms up
        } else {
            operateSolenoid(false);
        }

        putOnSmartDashboard();
    }
    
    public void teleopInit() {
        // new mechanisms
        timer = new Timer();
        climberTimer = new Timer();
        
        // various initial variable settings
        increment = 0;
        numShooterSpeedBumps = 0;
        
        // begin mechanism operation
        setBothShooterSpeeds(0.0);
        operateSolenoid(false);
        timer.start();
        climberTimer.start();
        compressor.start();
        robotDrive.drive(0.0, 0.0);
    }

    public void teleopPeriodic() {
        shoot();
        drive();
        climb();
        
        putOnSmartDashboard();

        print("shooter speed: " + shooter.get(),
                DriverStationLCD.Line.kUser1,
                true);
        print("speed increase: " + shooter.get(),
                DriverStationLCD.Line.kUser2,
                true);
    }

    public void operateSolenoid(boolean extend) {
        if (extend) {
            shooterSolenoid.set(DoubleSolenoid.Value.kReverse);
        } else {
            shooterSolenoid.set(DoubleSolenoid.Value.kForward);
        }
    }

    public void operateClimber(boolean extend) {
        if (extend) {
            climberSolenoid.set(DoubleSolenoid.Value.kReverse);
        } else {
            climberSolenoid.set(DoubleSolenoid.Value.kForward);
        }
    }

    public void putOnSmartDashboard() {
        SmartDashboard.putNumber("shooter speed", shooter.get());
        SmartDashboard.putNumber("shooter speed mod", increment);
        SmartDashboard.putNumber("timer", timer.get());
        SmartDashboard.putBoolean("compressor enabled: ", compressor.enabled());
        SmartDashboard.putString("shooter solenoid", getSolenoidType(shooterSolenoid.get()));
        SmartDashboard.putString("climber solenoid", getSolenoidType(climberSolenoid.get()));
        SmartDashboard.putBoolean("pressure switch", compressor.getPressureSwitchValue());
        SmartDashboard.putBoolean("new increment hit valid", lastIncHit+0.1<timer.get());
        SmartDashboard.putNumber("climbing coefficient", climberCoefficient);
    }

    public String getSolenoidType(DoubleSolenoid.Value a) {
        if (a == DoubleSolenoid.Value.kForward) {
            return "kFoward";
        } else if (a == DoubleSolenoid.Value.kOff) {
            return "kOff";
        }
        
        return "kReverse";
    }
    
    public void shoot() {
        if (timer.get() > 2) {
            // if trigger pulled, extend piston
            operateSolenoid(leftStick.getRawButton(JBUTTON_1));
            
            if (leftStick.getRawButton(JBUTTON_2)){
                currSpeed = backOfPyramidSpeed;
            }
            
            // this section bumps the shooter value up/down
            // while building in delays to allow the driver to
            // set more precise values
            if (leftStick.getRawButton(JBUTTON_4) &&
                    lastIncHit+0.1<timer.get()) {
                 bump(0.01);
                 lastIncHit=timer.get();
            } else if (leftStick.getRawButton(JBUTTON_5) &&
                    lastIncHit+0.1<timer.get()) {
                 bump(-0.01);
                 lastIncHit=timer.get();
            }

            // turns off shooters
            if (rightStick.getRawButton(JBUTTON_9)) {
                setBothShooterSpeeds(0);
                currSpeed = 0;
            }

            // this section makes sure that the shooter speed stays
            // within the bound 0..1
            if (currSpeed != 0) {
                if (currSpeed + increment <= 1 && currSpeed + increment >= 0) {
                    setBothShooterSpeeds(currSpeed+increment);
                }
                else if (currSpeed + increment > 1) {
                    increment = 1 - currSpeed;
                }
                else if (currSpeed + increment < 0) {
                    increment = -currSpeed;
                }
            }
        }
   }

    public void bump(double bump) {
        increment += bump;
    }
    
    public void reset() {
        increment = 0;
    }
    
    public void setBothShooterSpeeds(double speed) {
        shooter.set(-speed);
    }
    
    public void climb() {
        // if the button is pressed and hasn't been pressed recently (0.5 secs)
        if (rightStick.getRawButton(JBUTTON_1) && climberTimer.get() > 0.5) {
            climbing = !climbing;
            climberTimer.reset();
        }
        
        if (climbing) {
            operateClimber(true);
            setBothShooterSpeeds(0); // this wastes power and can jerk the robot
            climberCoefficient = 0.75;
        } else {
            operateClimber(false);
            climberCoefficient = 1;
        }
    }

    public void drive() {
        // the code below codes for an acceleration,
        // instead of an abrupt jerk
        // (which may lead to the robot toppling over)
        // if the diff. between the leftStickValue and
        // the lastleftStickValue is greater than 0.5
        if (Math.abs(leftStick.getY() - previousLeftDriveSpeed) > 0.3) {
            // set the lastLeft to this value here
            // (sets it to a calibration ratio; false average)
            previousLeftDriveSpeed = ((leftStick.getY() +
                    previousLeftDriveSpeed * 6) / 7);
        } else {
            // otherwise set lastLeft to the leftstick value
            previousLeftDriveSpeed = leftStick.getY();
        }
        // do the same for the right stick
        if (Math.abs(rightStick.getY() - previousRightDriveSpeed) > 0.3) {
            previousRightDriveSpeed = ((rightStick.getY() +
                    previousRightDriveSpeed * 6) / 7);
        } else {
            previousRightDriveSpeed = rightStick.getY();
        }

        robotDrive.tankDrive(previousLeftDriveSpeed*0.95*climberCoefficient,
                             previousRightDriveSpeed*0.95*climberCoefficient);
        // added the *0.95 because of concern from drive team about Jaguars
        // shutting down suddenly (for a split second) when jerked to 100% power
    }

    // this does some nicer formatting and prints to the driver station console
    public void print(String s, DriverStationLCD.Line n, boolean toLCD) {
        if (toLCD) {
            lcd.println(n, 1, "                                 ");
            lcd.println(n, 1, s);
            lcd.updateLCD();
        } else {
            System.out.println(s);
        }
    }
}



/**
 * This code was written by FIRST FRC Team 3501 (the Firebirds) of Fremont High
 * School in Sunnyvale, California. This is for the 2013 FRC season's game,
 * Ultimate Ascent.
 *
 *
 * ~ Jaidev P. Bapat, Software Team Lead
 */
