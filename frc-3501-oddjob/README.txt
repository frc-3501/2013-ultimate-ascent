This is the repository for the 2012-2013 Fremont High School robotics team.

-- RULES --

The "master" branch is solely for the final robot code.

Feel free to create /local/ branches for testing.

-- WHO TO CONTACT WITH QUESTIONS --

Logan, Daniel Watson, or Mr. Dobervich.
